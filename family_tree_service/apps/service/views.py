from family_tree_service.libs.utils.viewsets import ReadOnlyModelViewSet, GenericViewSet
from family_tree_service.libs.utils.mixins import ListModelMixin, CreateModelMixin
from .serializers import (UserPDFFamilysSerializer, FamiyPeopleSerializer, PDFFamilyPeopleSerializer,
                          IsAuthenticatedSerializer)
from .models import GenerFamily, GenerPerson, GenerUser
from .filters import FamilySearch, PersonSearch, PDFPeopleSearch
from django_filters.rest_framework import DjangoFilterBackend
from family_tree_service.libs.utils.tools import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import status
from family_tree_service.libs.middlewares.CustomPermission import AdminPermission, PdfPermission
from rest_framework.decorators import action
from django.http import HttpResponse

# Create your views here.

class UserPDFFamilysView(GenericViewSet, ListModelMixin):
    queryset = GenerFamily.objects.filter(is_delete=False).order_by('-pk')
    serializer_class = UserPDFFamilysSerializer
    filter_backends = [DjangoFilterBackend]
    filter_class = FamilySearch


    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset().filter(
            permission_family__user_id=request.user,
            permission_family__permission__gte=400).distinct())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class FamilyPeopleView(GenericViewSet, ListModelMixin):
    queryset = GenerPerson.objects.filter(is_delete=False).order_by('pk')
    serializer_class = FamiyPeopleSerializer
    filter_backends = [DjangoFilterBackend]
    filter_class = PersonSearch
    permission_classes = [AdminPermission]

    def list(self, request, *args, **kwargs):
        # family_id = request.query_params.get('family_id')
        # if not family_id:
        #     return Response('缺少family_id', status=status.HTTP_400_BAD_REQUEST)
        if not request.query_params.get('level'):
            return Response('缺少level', status=status.HTTP_400_BAD_REQUEST)
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            parents_ids = []
            for i in serializer.data:
                if i['father'] is not None:
                    parents_ids.append(i['father'])
                if i['mother'] is not None:
                    parents_ids.append(i['mother'])

            parents_name = GenerPerson.objects.filter(id__in=parents_ids,
                                                      type=1).values('id', 'name')
            parents_name = {str(i['id']): i['name'] for i in parents_name}
            for i, v in enumerate(serializer.data):
                parent_name = parents_name.get(v['father'], None)
                serializer.data[i]['parent_name'] = parent_name if parent_name else parents_name.get(v['mother'], '')
                del serializer.data[i]['father']
                del serializer.data[i]['mother']

            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        parents_ids = []
        for i in serializer.data:
            if i['father'] is not None:
                parents_ids.append(i['father'])
            if i['mother'] is not None:
                parents_ids.append(i['mother'])

        parents_name = GenerPerson.objects.filter(id__in=parents_ids,
                                                  type=1).values('id', 'name')
        parents_name = {str(i['id']): i['name'] for i in parents_name}
        for i, v in enumerate(serializer.data):
            parent_name = parents_name.get(v['father'], None)
            serializer.data[i]['parent_name'] = parent_name if parent_name else parents_name.get(v['mother'], '')
            del serializer.data[i]['father']
            del serializer.data[i]['mother']

        return Response(serializer.data)


class PdfServiceData(GenericViewSet):
    queryset = GenerPerson.objects.filter(is_delete=False).order_by('family_index')
    serializer_class = PDFFamilyPeopleSerializer
    filter_backends = [DjangoFilterBackend]
    filter_class = PDFPeopleSearch
    permission_classes = [AllowAny]

    @action(methods=['GET'], detail=True, url_path='data')
    def getAllFamilyPersonsOrderByFamilyIndex(self, request, *args, **kwargs):
        start_person = self.request.query_params.get('start_person')
        family_id = self.request.query_params.get('family_id')
        if family_id is None:
            return Response('缺少family_id', status=status.HTTP_400_BAD_REQUEST)

        if start_person is None:
            queryset = self.filter_queryset(self.get_queryset().filter(family_id=family_id))
        else:
            start_person = GenerPerson.objects.filter(pk=start_person).first()
            start_index = start_person.family_index
            last_index = start_person.get_last_family_index()
            queryset = self.filter_queryset(self.get_queryset().filter(family_id=family_id,
                                                                       family_index__range=[start_index, last_index]))



        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class PermissionCheckView(GenericViewSet, CreateModelMixin):
    serializer_class = IsAuthenticatedSerializer
    queryset = GenerUser.objects.filter(is_delete=False).order_by('id')
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        return Response({'user_id': request.user.pk})

    @action(methods=['POST'], detail=False, url_path='permission', permission_classes=[AdminPermission])
    def check_permission(self, request, *args, **kwargs):
        return Response({'user_id': request.user.pk})

    @action(methods=['POST'], detail=False, url_path='pdf_permission', permission_classes=[PdfPermission])
    def check_pdf_permission(self, request, *args, **kwargs):
        return Response({'user_id': request.user.pk})


def health(request):
    return HttpResponse('')