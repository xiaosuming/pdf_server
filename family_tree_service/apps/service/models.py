from django.db import models

# Create your models here.
# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.db.models import Min


class GenerUser(models.Model):
    user_type = models.IntegerField(default=1)
    username = models.CharField(max_length=45)
    nickname = models.CharField(max_length=45,null=True)
    phone = models.CharField(max_length=30, null=True)
    email = models.CharField(max_length=255, null=True)
    password = models.CharField(max_length=255)
    openid = models.CharField(max_length=100, blank=True, null=True)
    mini_openid = models.CharField(max_length=100, blank=True, null=True)
    ai_miniopenid = models.CharField(db_column='aiMiniOpenId', max_length=100, blank=True, null=True)  # Field name made lowercase.
    wx_unionid = models.CharField(db_column='wx_unionId', max_length=100, blank=True, null=True)  # Field name made lowercase.
    qq_openid = models.CharField(max_length=100, blank=True, null=True)
    wb_openid = models.CharField(max_length=100, blank=True, null=True)
    hx_username = models.CharField(max_length=255, blank=True, null=True)
    hx_password = models.CharField(max_length=255, blank=True, null=True)
    photo = models.CharField(max_length=255, blank=True, null=True)
    gender = models.IntegerField(default=2)
    birthday = models.DateField(blank=True, null=True)
    country = models.IntegerField(default=0)
    country_name = models.CharField(max_length=100, default=0)
    province = models.IntegerField(default=0)
    province_name = models.CharField(max_length=100, default=0)
    city = models.IntegerField(default=0)
    city_name = models.CharField(max_length=100, default=0)
    area = models.IntegerField(default=0)
    area_name = models.CharField(max_length=100, default=0)
    town = models.IntegerField(default=0)
    town_name = models.CharField(max_length=100, default=0)
    address = models.CharField(max_length=100, blank=True, null=True)
    is_delete = models.IntegerField(default=False)
    target_user = models.IntegerField(blank=True, null=True)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)
    regis_ip = models.CharField(max_length=45)
    regis_port = models.CharField(max_length=45)
    real_name = models.CharField(db_column='realName', max_length=50,default='')  # Field name made lowercase.
    father_name = models.CharField(db_column='fatherName', max_length=50, default='')  # Field name made lowercase.
    birth_place = models.CharField(db_column='birthPlace', max_length=100, default='')  # Field name made lowercase.
    marital_status = models.IntegerField(db_column='maritalStatus', default=0)  # Field name made lowercase.
    pdf_permission = models.IntegerField(default=0)

    REQUIRED_FIELDS = ['phone']
    USERNAME_FIELD = 'id'

    @property
    def is_anonymous(self):
        """
        Always return False. This is a way of comparing User objects to
        anonymous users.
        """
        return False

    @property
    def is_authenticated(self):
        """
        Always return True. This is a way to tell if the user has been
        authenticated in templates.
        """
        return True

    class Meta:
        managed = False
        db_table = 'gener_user'


class GenerPerson(models.Model):
    id = models.BigIntegerField(primary_key=True)
    same_person_id = models.BigIntegerField(db_column='same_personId')  # Field name made lowercase.
    infocard_id = models.IntegerField(db_column='infoCardId', blank=True, null=True)  # Field name made lowercase.
    branch_id = models.IntegerField(db_column='branchId', default=0)  # Field name made lowercase.
    family_id = models.ForeignKey('GenerFamily',db_column='familyId',db_constraint=False, related_name='person_family',
                                  on_delete=models.DO_NOTHING)  # Field name made lowercase.
    user_id = models.IntegerField(db_column='userId', default=0,)  # Field name made lowercase.
    type = models.IntegerField(default=1)
    is_delete = models.IntegerField(default=False)
    name = models.CharField(max_length=30)
    zpname = models.CharField(max_length=30, blank=True, null=True)
    birthday = models.DateField(blank=True, null=True)
    zi = models.CharField(max_length=10, blank=True, null=True)
    gender = models.IntegerField()
    photo = models.CharField(max_length=250)
    remark = models.CharField(max_length=255, blank=True, null=True)
    share_url = models.CharField(max_length=250)
    is_dead = models.IntegerField(default=0)
    is_adoption = models.IntegerField(default=0)
    dead_time = models.DateField(blank=True, null=True)
    ref_family_id = models.CharField(db_column='ref_familyId', max_length=100, blank=True, null=True)  # Field name made lowercase.
    ref_person_id = models.CharField(db_column='ref_personId', max_length=100, blank=True, null=True)  # Field name made lowercase.
    father = models.CharField(max_length=2000, default='')
    mother = models.CharField(max_length=2000, default='')
    son = models.CharField(max_length=2000, default='')
    daughter = models.CharField(max_length=2000, default='')
    spouse = models.CharField(max_length=2000, default='')
    sister = models.CharField(max_length=2000, default='')
    brother = models.CharField(max_length=2000, default='')
    level = models.IntegerField(default=0)
    confirm = models.IntegerField(blank=True, null=True)
    blood_type = models.IntegerField(blank=True, null=True)
    marital_status = models.IntegerField(default=0)
    phone = models.CharField(max_length=20, blank=True, null=True)
    qq = models.CharField(max_length=20, blank=True, null=True)
    qrcode = models.CharField(max_length=255, default='')
    profile_text = models.CharField(db_column='profileText', max_length=2000, default='')  # Field name made lowercase.
    side_text = models.CharField(db_column='sideText', max_length=100, default='')  # Field name made lowercase.
    country = models.IntegerField(default=0)
    country_name = models.CharField(max_length=100, default=0)
    province = models.IntegerField(default=0)
    province_name = models.CharField(max_length=100, default=0)
    city = models.IntegerField(default=0)
    city_name = models.CharField(max_length=100, default=0)
    area = models.IntegerField(default=0)
    area_name = models.CharField(max_length=100, default=0)
    town = models.IntegerField(default=0)
    town_name = models.CharField(max_length=100, default=0)
    address = models.CharField(max_length=100, blank=True, null=True)
    ranking = models.IntegerField(default=1)
    create_by = models.IntegerField()
    create_time = models.DateTimeField(auto_now_add=True)
    update_by = models.IntegerField()
    update_time = models.DateTimeField(auto_now=True)
    family_index = models.BigIntegerField(default=0)
    birth_place = models.CharField(db_column='birthPlace', max_length=120, default='')  # Field name made lowercase.
    burial_place = models.CharField(db_column='burialPlace', max_length=120, default='')  # Field name made lowercase.
    is_not_main = models.IntegerField(db_column='isNotMain', default=0)  # Field name made lowercase.
    real_mother = models.CharField(db_column='realMother', max_length=120, default='')  # Field name made lowercase.
    bicolor = models.BigIntegerField()
    logid = models.IntegerField(db_column='logId', default=0)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'gener_person'


    def get_last_family_index(self):
        count = GenerPerson.objects.filter(is_delete=False, type=1, level__lte=self.level, family_id=self.family_id,
                                           family_index__gt=self.family_index).aggregate(min=Min('family_index'))['min']
        if count is None:
            return GenerPerson.objects.filter(family_id=self.family_id, is_delete=False).values('family_index'
                                                                    ).order_by('-family_index').first()['family_index']
        return count - 1


class GenerLoginhistory(models.Model):
    login_name = models.CharField(max_length=255)
    device = models.CharField(max_length=30)
    device_code = models.CharField(max_length=100)
    user_id = models.IntegerField(db_column='userId')  # Field name made lowercase.
    token = models.CharField(max_length=64)
    is_logout = models.IntegerField(default=0)
    login_ip = models.CharField(max_length=56)
    login_port = models.CharField(max_length=45)
    login_time = models.DateTimeField()
    success = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'gener_loginHistory'


class GenerFamily(models.Model):
    name = models.CharField(max_length=100)
    surname = models.CharField(max_length=10, default='')
    min_level = models.IntegerField()
    max_level = models.IntegerField()
    # is_merge = models.IntegerField(default=0)
    permission = models.IntegerField()
    originator = models.IntegerField()
    member_count = models.IntegerField()
    description = models.CharField(max_length=100)
    photo = models.CharField(max_length=255)
    identity = models.CharField(max_length=100, blank=True, null=True)
    qrcode = models.CharField(max_length=255, blank=True, null=True)
    hx_chatroom = models.CharField(max_length=100, blank=True, null=True)
    is_delete = models.IntegerField(default=False)
    open_type = models.IntegerField(default=0)
    type = models.IntegerField(default=1)
    group_type = models.IntegerField(db_column='groupType',default=1)  # Field name made lowercase.
    socialcircleid = models.IntegerField(db_column='socialCircleId',default=0)  # Field name made lowercase.
    create_by = models.IntegerField()
    create_time = models.DateTimeField(auto_now_add=True)
    update_by = models.IntegerField()
    update_time = models.DateTimeField(auto_now=True)
    start_level = models.IntegerField(default=1)

    class Meta:
        managed = False
        db_table = 'gener_family'



class GenerPermission(models.Model):
    family_id = models.ForeignKey(GenerFamily, db_constraint=False, related_name="permission_family",
                                       on_delete=models.DO_NOTHING,db_column='familyId')  # Field name made lowercase.
    start_person_id = models.BigIntegerField(db_column='start_personId')  # Field name made lowercase.
    user_id = models.ForeignKey(GenerUser, db_column='userId', db_constraint=False,
                                on_delete=models.DO_NOTHING,related_name="permission_user")  # Field name made lowercase.
    permission = models.IntegerField(db_column='permissionType', default=0)  # Field name made lowercase.
    is_delete = models.IntegerField(default=False)
    create_time = models.DateTimeField(auto_now_add=True)
    create_by = models.BigIntegerField()
    update_time = models.DateTimeField(auto_now=True)
    update_by = models.BigIntegerField()
    name = models.CharField(max_length=50, default='')
    father_name = models.CharField(max_length=50, default='')
    person_id = models.ForeignKey(GenerPerson, db_column='personId', db_constraint=False, related_name="permission_person",
                                       on_delete=models.DO_NOTHING,default=0)  # Field name made lowercase.
    start_index = models.IntegerField(db_column='startIndex', default=0)  # Field name made lowercase.
    end_index = models.IntegerField(db_column='endIndex', default=0)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'gener_permission'

class GenerArea(models.Model):
    area_id = models.IntegerField(db_column='areaid',primary_key=True)
    area_name = models.CharField(db_column='areaname',max_length=50, blank=True, null=True)
    parent_id = models.IntegerField(db_column='parentId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'gener_area'


class GenerPersonInfo(models.Model):
    occupation = models.IntegerField(default=0)
    education = models.IntegerField(default=0)
    political_status = models.IntegerField(default=0)
    is_delete = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)
    create_by = models.IntegerField()
    update_by = models.IntegerField()
    person = models.OneToOneField(GenerPerson, db_column='person_id', db_constraint=False,db_index=True,
                                     on_delete=models.DO_NOTHING, related_name='other_info')
    # occupation

    class Meta:
        managed = False
        db_table = "gener_person_info"

class GenerFamilyPublicInformation(models.Model):
    title = models.CharField(max_length=255, default='')
    link_url = models.URLField()
    is_delete = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)
    create_by = models.IntegerField()
    update_by = models.IntegerField()

    class Meta:
        db_table = "gener_family_public_information"