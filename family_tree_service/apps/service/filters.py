from django_filters import rest_framework as filters
from family_tree_service.apps.service.models import GenerUser, GenerPerson, GenerFamily
from django.db.models import Q
from rest_framework.exceptions import ValidationError


class FamilySearch(filters.FilterSet):
    permission = filters.CharFilter(method='permission_filter')
    text = filters.CharFilter(method='text_filter')

    def permission_filter(self, queryset, name, value):
        return queryset
        # return queryset.filter(permission_family__permission__gte=400)

    def text_filter(self, queryset, name, value):
        return queryset.filter(Q(pk__icontains=value) | Q(name__icontains=value))


    class Meta:
        model = GenerFamily
        fields = 'permission',


class PersonSearch(filters.FilterSet):
    level = filters.CharFilter(field_name='level', lookup_expr='exact')
    name = filters.CharFilter(field_name='name', lookup_expr='icontains')
    family_id = filters.CharFilter(field_name='family_id', lookup_expr='exact')

    class Meta:
        model = GenerPerson
        fields = 'level', 'name', 'family_id'


class PDFPeopleSearch(filters.FilterSet):
    type = filters.CharFilter(method='type_filter')

    def type_filter(self, queryset, name, value):
        if value not in ['1', '0']:
            return queryset
        return queryset.filter(type=value)

    class Meta:
        model = GenerPerson
        fields = 'type',