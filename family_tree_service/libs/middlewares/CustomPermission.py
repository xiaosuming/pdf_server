from rest_framework.permissions import BasePermission
from family_tree_service.apps.service.models import GenerPermission
from django.contrib.auth.models import AnonymousUser
from rest_framework import HTTP_HEADER_ENCODING

class NormalPermission(BasePermission):
    message = "用户权限错误"

    def has_permission(self, request, view):
        if isinstance(request.user,AnonymousUser) or not request.user:
            return False
        family_id = self.get_family_id(request,view)
        if family_id is None:
            return False

        if not GenerPermission.objects.filter(family_id=family_id, user_id=request.user, is_delete=False).exists():
            return False

        return True

    def get_family_id(self, request, view):
        family_id = request.META.get('HTTP_FAMILYID', b'')
        if not family_id:
            family_id = request.query_params.get('family_id') if request.query_params else request.data.get('family_id')
        if family_id is None:
            try:
                family_id = view.kwargs.get(view.lookup_field)
            except AttributeError:
                pass

        try:
            int(family_id)
        except (ValueError, TypeError):
            return None
        return family_id



class AdminPermission(NormalPermission):
    message = "用户权限错误"

    def has_permission(self, request, view):
        if isinstance(request.user, AnonymousUser) or not request.user:
            return False
        family_id = self.get_family_id(request, view)
        print(48, family_id)
        if family_id is None:
            return False

        if not GenerPermission.objects.filter(family_id=family_id, user_id=request.user, permission__gte=400, is_delete=False).exists():
            from django.db import connection
            print(connection.queries)
            return False

        return True


class PdfPermission(NormalPermission):
    message = "用户权限错误"

    def has_permission(self, request, view):
        if isinstance(request.user, AnonymousUser) or not request.user:
            return False
        if not request.user.pdf_permission:
            return False
        family_id = self.get_family_id(request, view)
        if family_id is None:
            return False

        if not GenerPermission.objects.filter(family_id=family_id, user_id=request.user, permission__gte=400,
                                              is_delete=False).exists():
            from django.db import connection
            print(connection.queries)
            return False

        return True

