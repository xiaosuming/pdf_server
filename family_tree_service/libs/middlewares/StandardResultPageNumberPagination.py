from urllib import request

from rest_framework.pagination import PageNumberPagination
from family_tree_service.libs.utils.tools import Response
from django.core.paginator import InvalidPage
from rest_framework.exceptions import NotFound
import math


class StandardResultPageNumberPagination(PageNumberPagination):
    """
    自定义分页类
    """
    page_size = 10

    def __init__(self):
        super(StandardResultPageNumberPagination, self).__init__()
        self.page_size_query_param = 'page_size'  # 自定义分页的每页大小
        self.max_page_size = 100  # 这个设置很重要,表示最大每页大小

    def get_paginated_response(self, data):

        result = {
            'count': self.page.paginator.count,
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'page': self.page.previous_page_number() + 1 if self.page.has_previous() else 1
        }
        if type(data) is dict:
            result = dict(result, **data)
        else:
            result['result'] = data

        return Response(result)

    def paginate_queryset(self, queryset, request, view=None):
        """
        Paginate a queryset if required, either returning a
        page object, or `None` if pagination is not configured for this view.
        """
        page_size = self.get_page_size(request)
        if not page_size:
            return None

        paginator = self.django_paginator_class(queryset, page_size)
        page_number = request.query_params.get(self.page_query_param, 1)
        if page_number in self.last_page_strings:
            page_number = paginator.num_pages

        try:
            self.page = paginator.page(page_number)
        except InvalidPage as exc:
            # self.page = paginator.page(1)
            msg = self.invalid_page_message.format(
                page_number=page_number, message=str(exc)
            )
            raise NotFound(msg)

        if paginator.num_pages > 1 and self.template is not None:
            # The browsable API should display pagination controls.
            self.display_page_controls = True

        self.request = request
        return list(self.page)


class NewPageNumberPagination(PageNumberPagination):

    page_size = 10

    def __init__(self):
        super(NewPageNumberPagination, self).__init__()
        self.page_size_query_param = 'page_size'  # 自定义分页的每页大小
        self.max_page_size = 100  # 这个设置很重要,表示最大每页大小

    def get_paginated_response(self, data):

        if self.queryset_type == 'dict':
            count = self.dict_queryset_count
            page_number = self.page_number
            page_size = self.page_size
            pages = math.ceil(count / page_size)
            result = {
                'count': count,
                'next':  page_number - 1 if page_number > 1 else None,
                'previous': page_number + 1 if pages >= page_number + 1 else None,
                'page': page_number
            }
            if type(data) is dict:
                result = dict(result, **data)
            else:
                result['result'] = data

            return Response(result)
        else:
            result = {
                'count': self.page.paginator.count,
                'next': True if self.page.has_next() else False,
                'previous': True if self.page.has_previous() else False,
                'page': self.page.previous_page_number() + 1 if self.page.has_previous() else 1
            }
            if type(data) is dict:
                result = dict(result, **data)
            else:
                result['result'] = data

            return Response(result)

    def paginate_queryset(self, queryset, request, view=None):
        """
        Paginate a queryset if required, either returning a
        page object, or `None` if pagination is not configured for this view.
        """
        values = request.queryset_values if  hasattr(request, 'queryset_values') else None
        # print(values)
        if isinstance(queryset, dict) and isinstance(values, list):
            page_size = self.get_page_size(request)
            if not page_size:
                return None

            page_number = request.query_params.get(self.page_query_param, 1)
            offset = (page_number - 1) * page_size
            self.dict_queryset_count = queryset.count()
            self.page_number = page_number
            self.queryset_type = 'dict'
            return queryset.values(*values)[offset:offset + page_number]


        else:
            page_size = self.get_page_size(request)
            if not page_size:
                return None

            paginator = self.django_paginator_class(queryset, page_size)
            page_number = request.query_params.get(self.page_query_param, 1)
            if page_number in self.last_page_strings:
                page_number = paginator.num_pages

            try:
                self.page = paginator.page(page_number)
            except InvalidPage as exc:
                self.page = paginator.page(1)
                # msg = self.invalid_page_message.format(
                #     page_number=page_number, message=str(exc)
                # )
                # raise NotFound(msg)

            if paginator.num_pages > 1 and self.template is not None:
                # The browsable API should display pagination controls.
                self.display_page_controls = True

            self.request = request
            self.queryset_type = 'queryset'
            return list(self.page)