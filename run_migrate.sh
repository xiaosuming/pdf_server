#!/bin/bash
text=$*
echo $text
export DJANGO_SETTINGS_MODULE=family_tree_service.settings.$text
#python3 manage.py makemigrations --database=management --settings=laws_online.settings.uat
python3 manage.py migrate  --settings=family_tree_service.settings.$text
#python3 manage.py migrate --database=management --settings=family_tree_service.settings.$text
#python3 manage.py collectstatic --settings=family_tree_service.settings.$text --noinput
#
#python3 manage.py crontab remove --settings=family_tree_service.settings.uat
#python3 manage.py crontab add --settings=family_tree_service.settings.uat
#export C_FORCE_ROOT=True
#nohup celery --app=family_tree_service worker -l INFO  >/app/family_tree_service/logs/celery_worker.log 2>&1 &
#nohup celery -A family_tree_service beat -l info >/app/family_tree_service/logs/celery_beat.log 2>&1 &
#python3 manage.py rebuild_index --noinput
#python3 manage.py runserver 0.0.0.0:8000 --settings=family_tree_service.settings.$text
gunicorn -c gunicorn.conf.py --env DJANGO_SETTINGS_MODULE=family_tree_service.settings.$text family_tree_service.wsgi:application

#cd /app/family_tree_service/family_tree_service
#sleep 5
#nohup celery -A family_tree_service beat -l info >/app/family_tree_service/log/celery_beat.log 2>&1 &

#set -x
#env >> /etc/default/locale
#nohup python3 manage.py runserver 0:8000  > run.log 2>&1 &
#nohup celery worker -A matrix -l info  >celery.log 2>&1 &
#nohup celery -A matrix beat -l info  >celery_beat.log 2>&1 &
