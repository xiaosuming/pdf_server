from rest_framework import serializers
from rest_framework.serializers import ValidationError
from family_tree_service.apps.service.models import GenerUser, GenerPerson, GenerFamily
from family_tree_service.libs.utils.tools import ContextUser
from django.contrib.auth.models import AnonymousUser
import re


class UserPDFFamilysSerializer(serializers.ModelSerializer):

    class Meta:
        fields = 'id', 'name', 'photo', 'surname', 'min_level', 'max_level', 'member_count'
        model = GenerFamily


class FamiyPeopleSerializer(serializers.ModelSerializer):
    id = serializers.CharField()
    father = serializers.SerializerMethodField()
    mother = serializers.SerializerMethodField()

    def get_father(self, obj):
        father = re.findall(r'@(.*?)\|', obj.father)
        return father[0] if father else None

    def get_mother(self, obj):
        mother = re.findall(r'@(.*?)\|', obj.mother)
        return mother[0] if mother else None

    class Meta:
        fields = 'id', 'name', 'photo', 'father', 'ranking', 'type', 'mother', 'gender'
        model = GenerPerson


class PDFFamilyPeopleSerializer(serializers.ModelSerializer):
    id = serializers.CharField()
    familyIndex = serializers.IntegerField(source='family_index')
    profileText = serializers.CharField(source='profile_text')
    isAdoption = serializers.IntegerField(source='is_adoption')
    sideText = serializers.CharField(source='side_text')
    father = serializers.SerializerMethodField()
    mother = serializers.SerializerMethodField()
    son = serializers.SerializerMethodField()
    daughter = serializers.SerializerMethodField()
    spouse = serializers.SerializerMethodField()

    def get_father(self, obj):
        return re.findall(r'@(.*?)\|', obj.father)

    def get_mother(self, obj):
        return re.findall(r'@(.*?)\|', obj.mother)

    def get_son(self, obj):
        return re.findall(r'@(.*?)\|', obj.son)

    def get_daughter(self, obj):
        return re.findall(r'@(.*?)\|', obj.daughter)

    def get_spouse(self, obj):
        return re.findall(r'@(.*?)\|', obj.spouse)

    class Meta:
        model = GenerPerson
        fields = ('id', 'name', 'level', 'father', 'mother', 'gender', 'son', 'daughter', 'type', 'spouse',
                  'ranking', 'birthday', 'address', 'profileText', 'photo', 'isAdoption', 'sideText', 'familyIndex')


class IsAuthenticatedSerializer(serializers.Serializer):
    pass