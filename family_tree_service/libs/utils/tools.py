# from rest_framework import status as s
from rest_framework.response import Response as R
from rest_framework.serializers import Serializer
from django_redis import get_redis_connection
from django.views.static import serve
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from django.contrib.auth.models import AnonymousUser

from rest_framework.permissions import IsAuthenticated
# from urllib.parse import quote,unquote

# 重定义Response

class Response(R):

    def __init__(self, data=None, status=None,
                 template_name=None, headers=None,
                 exception=False, content_type=None):
        """
        Alters the init arguments slightly.
        For example, drop 'template_name', and instead use 'data'.

        Setting 'renderer' and 'media_type' will typically be deferred,
        For example being set automatically by the `APIView`.
        """
        super().__init__(None, status=status)

        if isinstance(data, Serializer):
            msg = (
                'You passed a Serializer instance as data, but '
                'probably meant to pass serialized `.data` or '
                '`.error`. representation.'
            )
            raise AssertionError(msg)

        status = status if status else 200

        if 200 <= status < 300:
            self.data = {
                'code': 0,
                'msg': 'SUCCESS',
                'data': data
            }
        else:
            self.data = {
                'code': status,
                'msg': data,
                'data': ''
            }
        self.template_name = template_name
        self.exception = exception
        self.content_type = content_type


#X-Forwarded-For:简称XFF头，它代表客户端，也就是HTTP的请求端真实的IP，只有在通过了HTTP 代理或者负载均衡服务器时才会添加该项。
def get_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]#所以这里是真实的ip
    else:
        ip = request.META.get('REMOTE_ADDR')#这里获得代理ip
    return ip



def dict_flatten(items):
    for k, v in items.items():
        if isinstance(v, dict):
            yield from dict_flatten(v)
        else:
            yield k, v


class ContextUser:
    def set_context(self, serializer_field):
        self.user = serializer_field.context['request'].user

    def __call__(self):
        return self.user

# @api_view(['GET'])
# @authentication_classes([CustomJSONWebTokenAuthentication, SessionAuthentication, BasicAuthentication])
# def custom_serve(request, path, document_root=None, show_indexes=False):
#     user = request.user
#     print(request.get_full_path())
#     full_path = request.get_full_path()
#     full_path_list = full_path.split('/')
#     if full_path_list[2] not in ['advertisement', 'conversation']:
#         if isinstance(user, AnonymousUser):
#             return Response('', status=401)
#
#     file_name = AdvertisementPicture.objects.filter(is_deleted=False, picture=full_path_list[-1]).first()
#     path = file_name.picture.path
#
#     return serve(request, path, document_root=MEDIA_ROOT, show_indexes=False)



if __name__ == '__main__':
    pass
    # city_model(data['districts'])
