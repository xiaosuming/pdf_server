FROM python:3.8-slim
ENV PYTHONBUFFERED 1
ARG branchTag=${branchTag}
ENV branchTag ${branchTag}

RUN sed -i 's@deb.debian.org@mirrors.aliyun.com@g' /etc/apt/sources.list \
    && apt-get update \
    && apt-get upgrade -y \
    && apt-get -y install vim \
    && apt-get install -y  build-essential libssl-dev libffi-dev libgl1-mesa-glx \
    && apt-get install -y python3-dev default-libmysqlclient-dev \
    && mkdir -p /app/family_tree_service/ \
    && cd /app/family_tree_service/ \
    && mkdir logs\
    && mkdir media \
    && rm -r /var/lib/apt/lists/*

ENV CONFIG_FILE family_tree_service.settings.uat.py
WORKDIR /app/family_tree_service/

COPY ./family_tree_service/requirements/base.txt /app/family_tree_service/

RUN pip install -i https://mirrors.aliyun.com/pypi/simple/ --trusted-host mirrors.aliyun.com -r /app/family_tree_service/base.txt \
    && rm -rf ~./cache/pip \
    && rm -rf /tmp

COPY ./ /app/family_tree_service/
EXPOSE 8000

RUN echo "$branchTag"
RUN chmod +x ./run_migrate.sh
CMD ["/bin/sh", "-c", "./run_migrate.sh $branchTag"]