import datetime
from django.utils.translation import ugettext_lazy
from django.core.cache import cache
from rest_framework.authentication import BaseAuthentication
from rest_framework.authentication import BasicAuthentication
from rest_framework import exceptions
from rest_framework.authtoken.models import Token
from rest_framework import HTTP_HEADER_ENCODING
from django.utils import timezone
from family_tree_service.apps.service.models import GenerUser, GenerLoginhistory
from django_redis import get_redis_connection


# from matrix.utils.tools import get_local_time

# 获取请求头信息
def get_authorization_header(request):
    auth = request.META.get('HTTP_AUTHORIZATION', b'')
    # auth = auth if auth else request.query_params('id_token')
    # auth = request.META.get('HTTP_ID_TOKEN', b'')
    # print(f'auth: {auth}')
    if isinstance(auth, type('')):
        auth = auth.encode(HTTP_HEADER_ENCODING)
    # print(f'auth: {auth}')
    return auth


# 自定义认证方式，这个是后面要添加到设置文件的

class ExpiringTokenAuthentication(BasicAuthentication):
    model = Token

    def authenticate(self, request):
        # print(request.META)
        auth = get_authorization_header(request)
        if not auth:
            return None
        try:
            token = auth.decode()
        except UnicodeError:
            msg = ugettext_lazy("无效的Token， Token头不应包含无效字符")
            raise exceptions.AuthenticationFailed(msg)
        return self.authenticate_credentials(token)

    def authenticate_credentials(self, key):
        # 尝试从缓存获取用户信息（设置中配置了缓存的可以添加，不加也不影响正常功能）
        print(key)
        try:
            user_id, key, using_device = key.split('|')
        except ValueError:
            raise exceptions.AuthenticationFailed('token错误')
        using_device = int(using_device)
        # device_detail = 'PC'
        device_dict = {
            1: 'PC',
            2: 'mobile',
            3: 'miniprogram',
        }
        device_detail = device_dict.get(using_device)

        temp_storage = get_redis_connection('temp_storage')
        cache_user = temp_storage.get(key)
        cache_user = cache_user.decode() if cache_user else cache_user
        if cache_user != user_id and using_device == 1:
            # print(36,cache_user,type(cache_user))
            raise exceptions.AuthenticationFailed('认证失败')
            # return cache_user, key  # 这里需要返回一个列表或元组，原因不详

        # if device_detail in [LOGIN_DEVICE_WECHAT, LOGIN_DEVICE_BROWSER]:
        #     if not GenerLoginhistory.objects.filter(user_id=user_id, token=key,device=device_detail,is_logout=0,success=1).exists():
        #         raise exceptions.AuthenticationFailed('认证失败')
        # elif device_detail == LOGIN_DEVICE_APP:
        #     storage_token = GenerLoginhistory.objects.filter(user_id=user_id, token=key, device=device_detail,
        #                             is_logout=0, success=1).values('token').order_by('pk').first().get('token')
        #     if storage_token != key:
        #         raise exceptions.AuthenticationFailed('认证失败')
        # else:
        #     raise exceptions.AuthenticationFailed('认证失败')

        temp_storage.set(key, user_id, 7200)
        user = GenerUser.objects.filter(pk=user_id, is_delete=False).first()
        if not user:
            raise exceptions.AuthenticationFailed('认证失败')
        return user, key

    def authenticate_header(self, request):
        return 'Token'
