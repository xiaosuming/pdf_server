from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'family',
        'USER': 'family',
        'PASSWORD': 'Flare1111',
        'HOST': '47.111.172.152',
        'PORT': '3306',
        'OPTIONS': {
            "init_command": "SET foreign_key_checks = 0;",
        },
    },
}


CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://47.111.172.152:6379/0",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "PASSWORD": 'flare1111'
        },
        'TIMEOUT': 7200
    },
    "temp_storage": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://47.111.172.152:6382/0",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "PASSWORD": 'flare1111'
        },
        'TIMEOUT': 7200
    },
    "cache_storage": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://47.111.172.152:6381/3",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "PASSWORD": 'flare1111'
        },
        'TIMEOUT': 7200
    }
}
DEBUG=False