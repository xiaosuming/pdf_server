from django.urls import path, include
from family_tree_service.apps.service import views
from rest_framework import routers

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'familys', views.UserPDFFamilysView, 'familys')
router.register(r'family/people', views.FamilyPeopleView, 'family_people')
router.register(r'authentication', views.PermissionCheckView, 'authentication')
# router.register(r'family', views.GetFamilyInfoView, 'family_info')
# router.register(r'platform', views.GetPlatformDataView, 'platform')
# router.register(r'family_information', views.FamilyInformationView, 'family_public_information')


urlpatterns = [
    path('pdf_data/', include((router.urls, 'family_tree_service'), namespace='family_pdf')),
]
